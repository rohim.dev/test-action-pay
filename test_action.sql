PGDMP  :    2                |            test_action    15.1 (Debian 15.1-1.pgdg110+1)    16.0 .    I           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            J           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            K           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            L           1262    311560    test_action    DATABASE     v   CREATE DATABASE test_action WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';
    DROP DATABASE test_action;
                postgres    false            �            1259    311719    product_dimensions    TABLE     4  CREATE TABLE public.product_dimensions (
    id integer NOT NULL,
    product_id integer,
    width numeric(10,2),
    height numeric(10,2),
    depth numeric(10,2),
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
 &   DROP TABLE public.product_dimensions;
       public         heap    postgres    false            �            1259    311718    product_dimensions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_dimensions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.product_dimensions_id_seq;
       public          postgres    false    218            M           0    0    product_dimensions_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.product_dimensions_id_seq OWNED BY public.product_dimensions.id;
          public          postgres    false    217            �            1259    311799    product_images    TABLE     *  CREATE TABLE public.product_images (
    id integer NOT NULL,
    product_id integer,
    image_url character varying(255) DEFAULT NULL::character varying,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
 "   DROP TABLE public.product_images;
       public         heap    postgres    false            �            1259    311798    product_images_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_images_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.product_images_id_seq;
       public          postgres    false    224            N           0    0    product_images_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.product_images_id_seq OWNED BY public.product_images.id;
          public          postgres    false    223            �            1259    311756    product_reviews    TABLE     �  CREATE TABLE public.product_reviews (
    id integer NOT NULL,
    product_id integer,
    rating double precision DEFAULT 0,
    comment text,
    reviewer_name character varying(100) DEFAULT NULL::character varying,
    reviewer_email character varying(100) DEFAULT NULL::character varying,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
 #   DROP TABLE public.product_reviews;
       public         heap    postgres    false            �            1259    311755    product_reviews_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_reviews_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.product_reviews_id_seq;
       public          postgres    false    222            O           0    0    product_reviews_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.product_reviews_id_seq OWNED BY public.product_reviews.id;
          public          postgres    false    221            �            1259    311737    product_tags    TABLE     !  CREATE TABLE public.product_tags (
    id integer NOT NULL,
    product_id integer,
    tag character varying(50) DEFAULT NULL::character varying,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
     DROP TABLE public.product_tags;
       public         heap    postgres    false            �            1259    311736    product_tags_id_seq    SEQUENCE     �   CREATE SEQUENCE public.product_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.product_tags_id_seq;
       public          postgres    false    220            P           0    0    product_tags_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.product_tags_id_seq OWNED BY public.product_tags.id;
          public          postgres    false    219            �            1259    311694    products    TABLE     #  CREATE TABLE public.products (
    id integer NOT NULL,
    slug character varying(255),
    title character varying(255),
    description text,
    category character varying(100),
    price numeric(10,2) DEFAULT 0,
    discount_percentage numeric(5,2) DEFAULT 0,
    rating numeric(3,2) DEFAULT 0,
    stock integer DEFAULT 0,
    brand character varying(100) DEFAULT NULL::character varying,
    sku character varying(50) DEFAULT NULL::character varying,
    weight numeric(10,2) DEFAULT 0,
    warranty_information text,
    shipping_information text,
    availability_status character varying(50) DEFAULT NULL::character varying,
    return_policy text,
    minimum_order_quantity integer,
    thumbnail character varying(255) DEFAULT NULL::character varying,
    is_active boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    deleted_at timestamp without time zone,
    barcode character varying,
    qr_code character varying
);
    DROP TABLE public.products;
       public         heap    postgres    false            �            1259    311693    products_id_seq    SEQUENCE     �   CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.products_id_seq;
       public          postgres    false    216            Q           0    0    products_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;
          public          postgres    false    215            �            1259    311686    schema_migrations    TABLE     c   CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);
 %   DROP TABLE public.schema_migrations;
       public         heap    postgres    false            �           2604    311722    product_dimensions id    DEFAULT     ~   ALTER TABLE ONLY public.product_dimensions ALTER COLUMN id SET DEFAULT nextval('public.product_dimensions_id_seq'::regclass);
 D   ALTER TABLE public.product_dimensions ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    218    218            �           2604    311802    product_images id    DEFAULT     v   ALTER TABLE ONLY public.product_images ALTER COLUMN id SET DEFAULT nextval('public.product_images_id_seq'::regclass);
 @   ALTER TABLE public.product_images ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    224    224            �           2604    311759    product_reviews id    DEFAULT     x   ALTER TABLE ONLY public.product_reviews ALTER COLUMN id SET DEFAULT nextval('public.product_reviews_id_seq'::regclass);
 A   ALTER TABLE public.product_reviews ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    222    222            �           2604    311740    product_tags id    DEFAULT     r   ALTER TABLE ONLY public.product_tags ALTER COLUMN id SET DEFAULT nextval('public.product_tags_id_seq'::regclass);
 >   ALTER TABLE public.product_tags ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219    220            �           2604    311697    products id    DEFAULT     j   ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);
 :   ALTER TABLE public.products ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    216    216            @          0    311719    product_dimensions 
   TABLE DATA           j   COPY public.product_dimensions (id, product_id, width, height, depth, created_at, updated_at) FROM stdin;
    public          postgres    false    218   �<       F          0    311799    product_images 
   TABLE DATA           [   COPY public.product_images (id, product_id, image_url, created_at, updated_at) FROM stdin;
    public          postgres    false    224   n=       D          0    311756    product_reviews 
   TABLE DATA           �   COPY public.product_reviews (id, product_id, rating, comment, reviewer_name, reviewer_email, created_at, updated_at) FROM stdin;
    public          postgres    false    222   q>       B          0    311737    product_tags 
   TABLE DATA           S   COPY public.product_tags (id, product_id, tag, created_at, updated_at) FROM stdin;
    public          postgres    false    220   �@       >          0    311694    products 
   TABLE DATA           =  COPY public.products (id, slug, title, description, category, price, discount_percentage, rating, stock, brand, sku, weight, warranty_information, shipping_information, availability_status, return_policy, minimum_order_quantity, thumbnail, is_active, created_at, updated_at, deleted_at, barcode, qr_code) FROM stdin;
    public          postgres    false    216   �A       <          0    311686    schema_migrations 
   TABLE DATA           ;   COPY public.schema_migrations (version, dirty) FROM stdin;
    public          postgres    false    214   �C       R           0    0    product_dimensions_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.product_dimensions_id_seq', 15, true);
          public          postgres    false    217            S           0    0    product_images_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.product_images_id_seq', 10, true);
          public          postgres    false    223            T           0    0    product_reviews_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.product_reviews_id_seq', 30, true);
          public          postgres    false    221            U           0    0    product_tags_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.product_tags_id_seq', 30, true);
          public          postgres    false    219            V           0    0    products_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.products_id_seq', 23, true);
          public          postgres    false    215            �           2606    311726 *   product_dimensions product_dimensions_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.product_dimensions
    ADD CONSTRAINT product_dimensions_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.product_dimensions DROP CONSTRAINT product_dimensions_pkey;
       public            postgres    false    218            �           2606    311807 "   product_images product_images_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.product_images
    ADD CONSTRAINT product_images_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.product_images DROP CONSTRAINT product_images_pkey;
       public            postgres    false    224            �           2606    311768 $   product_reviews product_reviews_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.product_reviews
    ADD CONSTRAINT product_reviews_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.product_reviews DROP CONSTRAINT product_reviews_pkey;
       public            postgres    false    222            �           2606    311745    product_tags product_tags_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.product_tags
    ADD CONSTRAINT product_tags_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.product_tags DROP CONSTRAINT product_tags_pkey;
       public            postgres    false    220            �           2606    311713    products products_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.products DROP CONSTRAINT products_pkey;
       public            postgres    false    216            �           2606    311690 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);
 R   ALTER TABLE ONLY public.schema_migrations DROP CONSTRAINT schema_migrations_pkey;
       public            postgres    false    214            �           2606    311727 5   product_dimensions product_dimensions_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_dimensions
    ADD CONSTRAINT product_dimensions_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id);
 _   ALTER TABLE ONLY public.product_dimensions DROP CONSTRAINT product_dimensions_product_id_fkey;
       public          postgres    false    3233    216    218            �           2606    311808 -   product_images product_images_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_images
    ADD CONSTRAINT product_images_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id);
 W   ALTER TABLE ONLY public.product_images DROP CONSTRAINT product_images_product_id_fkey;
       public          postgres    false    224    3233    216            �           2606    311769 /   product_reviews product_reviews_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_reviews
    ADD CONSTRAINT product_reviews_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id);
 Y   ALTER TABLE ONLY public.product_reviews DROP CONSTRAINT product_reviews_product_id_fkey;
       public          postgres    false    216    3233    222            �           2606    311746 )   product_tags product_tags_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_tags
    ADD CONSTRAINT product_tags_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.products(id);
 S   ALTER TABLE ONLY public.product_tags DROP CONSTRAINT product_tags_product_id_fkey;
       public          postgres    false    216    3233    220            @   �   x����	C!F��d�.p%5:K���Z(�ڼ	'~����
�:7�(RQ��]�7�6��i���˨I,BD��8-��"u4�!���0�6�!�_Ki�����^������5b�>����5�F�Sb��Hj������{�4�������jI- ����^��PÍ�      F   �   x���;n�0��Y<E���DRO��Z���F�~���ܾ�H�Q�|�R(Ј˶-�S*����q���y�yղ������c:E�i�n꣔a��;�TrZ�1}�r9���zH)
�2�i2'�NDo�wȝ�����TH�m]9���N�]�*'��jW!0}�*����Uw+0��2���'06�2�aI��l��غ��v�iWA����#S� 
��U�8>��Ԃ�uVp�u��H �[Ų�      D   -  x���=o�0@gݯP~@����C��h�.���lɰd�ί/iƈ�B�f�|>R2���é<��z�?���a]�N��xX��y(�w���5�K�P���˿ju��N/}�V�nW�"~T���Tn�z���J9
3(�tCY��闇��f����m��k��m�V�iY=�/?'�@hg�$7y�}=l��MR�\ևm3����y=���z=�?'�^Y7��4R݃e���3���CP�����m�а�M�i$$��
��aj�.|�߿z48ϓwA�\�z��h=� U�N*�&�L3�^��fh�|F���2鵵�M]F���R��xC��+���Hoȳ��j.�E�2�3���&#��@z^��H����@����᳁=�щ�B�G���g����躹B�6��³RF����VB��`��]{��0�e�3+���(�J�7�N��Hg�DzT�����]&��/�gP�aH*��V��)���z�������� &��Αu7v~F@�)���{OL�G.#H��P��n��@�d���{��wF����d�      B   $  x�}�;n�0k����#EJ{�4N�2M>Enj��"��C?�$�ya]�>�ߟ�z+v^��Y�ZWw�cD|n����v]X�'�X��F/��Q8DD��!�� � �����-�&���Xs[�L2vD�K�D��_����i���Ǝ��1���z����LC�7���!Z�O4����)�ԢD�91��c'�d<A�f�"�	�ǫ�R{�Ӥ�.q!�����a; ���L�BYj�qPd�U��v@�6"2����>ʎ�}�i4�WII�FSco	�ו���Hl      >   �  x���=o�0���)��#�o������!�ڦc��lb�������t�R�e�`������P��]5ɠ\��Jz��d�]x��|I����UH�ۜB�lX~�s�`4��+�0p��IÝ��y�����w���t}���mt�G�FM��$��H`)P�\���Ⱦӏ�aB��h��IY�t����P��9AW�7�T����فm�du���3b�ޏn��U�q=���3WfHGk��.�ul\z).�k��ȼ��i��yR�N�A���c��b����	c@�=�{��dD�K��W$i.�(�e^����:�C�L�`����ӈ;�ERӍzE�2���L=G�y$5ۨW��1$8.E�E$5ߨW�f��/E�e$�بW��e!����(�Z�ږ�<�7�~G�b/8xeK��Y�)۠c�9	G�E@�U[�:ZdyF�f�j;�1�0�\��4-D�|�fl���.$-^�ϗ��̷x���n�'G      <      x�3202103220447��L����� 0��     