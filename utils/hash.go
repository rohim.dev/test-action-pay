package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func Hash(value string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(value), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}

func Verify(hashed, value string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(value)); err != nil {
		return false
	}

	return true
}
