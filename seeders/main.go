package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"test-actionpay/config"
	"test-actionpay/src/product/model"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/gosimple/slug"
)

var product = model.Product{
	Title:                "Essence Mascara Lash Princess",
	Description:          "The Essence masvar Lash Princess is a popular mascara known for its volumizing and lengthening",
	Category:             "beauty",
	Price:                9.99,
	DiscountPercentage:   7.17,
	Rating:               4.94,
	Stock:                5,
	Brand:                "Essnce",
	SKU:                  "RCH45Q1A",
	Weight:               2,
	WarrantyInformation:  "1 month warranty",
	ShippingInformation:  "Ships in 1 month",
	AvailabilityStatus:   "Low Stock",
	ReturnPolicy:         "30 days return policy",
	MinimumOrderQuantity: 24,
	Thumbnail:            "https://cdn.dummyjson.com/products/images/beauty/Essence%20Mascara%20Lash%20Princess/thumbnail.png",
	Dimensions:           model.Dimensions{Width: 23.17, Height: 14.43, Depth: 28.01},
	Meta: model.Meta{
		Barcode: "9164035109868",
		QRCode:  "https://dummyjson.com/public/qr-code.png",
	},
	Images: []string{"https://cdn.dummyjson.com/products/images/beauty/Essence%20Mascara%20Lash%20Princess/1.png"},
	Tags:   []string{"beauty", "mascara"},
	Reviews: []model.Review{
		{
			Rating:        2,
			Comment:       "Very unhappy with my purchase!",
			Date:          time.Now(),
			ReviewerName:  "John Doe",
			ReviewerEmail: "john.doe@x.dummyjson.com",
		},
		{
			Rating:        2,
			Comment:       "Not as described!",
			Date:          time.Now(),
			ReviewerName:  "Nolan Gonzalez",
			ReviewerEmail: "nolan.gonzalez@x.dummyjson.com",
		},
		{
			Rating:        5,
			Comment:       "Very satisfied!",
			Date:          time.Now(),
			ReviewerName:  "Scarlett Wright",
			ReviewerEmail: "scarlett.wright@x.dummyjson.com",
		},
	},
}

func main() {
	// Setup Configuration
	configuration := config.New()
	//Open DB
	postgre := config.NewPostgreSqlxDatabase(configuration)

	ctx := context.Background()
	for i := 0; i < 10; i++ {
		// Insert the product into the database
		var returnedID int
		query := sq.Insert("products").Columns(
			"title", "slug", "description", "category", "price", "discount_percentage", "rating", "stock", "brand", "sku", "weight",
			"warranty_information", "shipping_information", "availability_status",
			"return_policy", "minimum_order_quantity", "barcode", "qr_code", "thumbnail",
		).Values(
			fmt.Sprintf("%s - %d", product.Title, i), slug.Make(fmt.Sprintf("%s - %d", product.Title, i)), product.Description, product.Category, product.Price, product.DiscountPercentage, product.Rating, product.Stock,
			product.Brand, product.SKU, product.Weight, product.WarrantyInformation,
			product.ShippingInformation, product.AvailabilityStatus, product.ReturnPolicy, product.MinimumOrderQuantity,
			product.Meta.Barcode, product.Meta.QRCode, product.Thumbnail,
		).Suffix("RETURNING id").PlaceholderFormat(sq.Dollar)

		sql, args, err := query.ToSql()
		if err != nil {
			log.Fatalf("Failed to build query insert product: %v", err)
			return
		}

		err = postgre.DB.QueryRowContext(ctx, sql, args...).Scan(&returnedID)
		if err != nil {
			log.Fatalf("Failed to insert product: %v", err)
			return
		}

		// Insert the dimension into the database
		q := sq.Insert("product_dimensions").
			Columns("product_id", "width", "height", "depth").
			Values(returnedID, product.Dimensions.Width, product.Dimensions.Height, product.Dimensions.Depth).
			PlaceholderFormat(sq.Dollar)

		sql, args, err = q.ToSql()
		if err != nil {
			log.Fatalf("Failed to build query insert dimensions: %v", err)
			return
		}

		if _, err := postgre.DB.ExecContext(ctx, sql, args...); err != nil {
			log.Fatalf("Failed to insert dimension: %v", err)
			return
		}

		// Insert the tag into the database
		for _, v := range product.Tags {
			q = sq.Insert("product_tags").
				Columns("product_id", "tag").
				Values(returnedID, v).
				PlaceholderFormat(sq.Dollar)

			sql, args, err = q.ToSql()
			if err != nil {
				log.Fatalf("Failed to build query insert tag %d: %v", i, err)
				continue
			}

			if _, err := postgre.DB.ExecContext(ctx, sql, args...); err != nil {
				log.Fatalf("Failed to insert tag: %v %d", i, err)
				continue
			}
		}

		// Insert the image into the database
		for _, v := range product.Images {
			q = sq.Insert("product_images").
				Columns("product_id", "image_url").
				Values(returnedID, v).
				PlaceholderFormat(sq.Dollar)

			sql, args, err = q.ToSql()
			if err != nil {
				log.Fatalf("Failed to build query insert image %d: %v", i, err)
				continue
			}

			if _, err := postgre.DB.ExecContext(ctx, sql, args...); err != nil {
				log.Fatalf("Failed to insert image: %v %d", i, err)
				continue
			}
		}

		// Insert the review into the database
		for _, v := range product.Reviews {
			q = sq.Insert("product_reviews").
				Columns("product_id", "rating", "comment", "reviewer_name", "reviewer_email").
				Values(returnedID, v.Rating, v.Comment, v.ReviewerName, v.ReviewerEmail).
				PlaceholderFormat(sq.Dollar)

			sql, args, err = q.ToSql()
			if err != nil {
				log.Fatalf("Failed to build query insert reviews %d: %v", i, err)
				continue
			}

			if _, err := postgre.DB.ExecContext(ctx, sql, args...); err != nil {
				log.Fatalf("Failed to insert reviews: %v %d", i, err)
				continue
			}
		}
	}
	log.Println("seeders successfull")
	postgre.DB.Close()
	os.Exit(1)
}
