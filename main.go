package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"test-actionpay/config"
	"test-actionpay/exception"
	"test-actionpay/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"

	product_ctrl "test-actionpay/src/product/controller"
	product_repo "test-actionpay/src/product/repository"
	product_svc "test-actionpay/src/product/service"
)

func main() {
	// Setup Configuration
	configuration := config.New()
	serverConfig := config.NewServerConfig(configuration)

	//Open DB
	postgre := config.NewPostgreSqlxDatabase(configuration)

	// Initialize pkg
	utils.NewLogger()

	// Setup Repository
	productRepo := product_repo.NewProductRepository(postgre.DB)

	// Setup Service
	productService := product_svc.NewProductService(productRepo)

	// Setup Controller
	productCtrl := product_ctrl.NewProductController(productService)

	// Setup Fiber
	app := fiber.New(config.NewFiberConfig())
	app.Use(recover.New())
	app.Use(config.NewLogger(config.LoggerConfig{
		Logger: utils.Logger,
	}))

	// Setup Routing
	public := app.Group("/api")
	public.Get("/health-check", func(ctx *fiber.Ctx) error {
		return ctx.Status(200).JSON(map[string]interface{}{
			"code":   200,
			"status": "success",
			"data":   fmt.Sprintf("Version %s", configuration.Get("APP_VERSION")),
			"IP":     ctx.IP(),
			"IPs":    ctx.IPs(),
		})
	})

	//Register Rest API Route
	productCtrl.Route(public)

	// Start App
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		err := app.Listen(fmt.Sprintf(":%s", serverConfig.Port))
		if err != nil {
			exception.PanicIfNeeded(err)
		}
	}()

	// graceful shutdown
	<-stop
	log.Println("Stopping server...")
	postgre.DB.Close()
}
