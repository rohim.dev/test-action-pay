CREATE TABLE IF NOT EXISTS product_reviews (
    id serial primary key,
    product_id INT DEFAULT NULL,
    rating FLOAT DEFAULT 0,
    comment TEXT DEFAULT NULL,
    reviewer_name VARCHAR(100) DEFAULT NULL,
    reviewer_email VARCHAR(100) DEFAULT NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,

    FOREIGN KEY (product_id) REFERENCES products(id)
);
