CREATE TABLE IF NOT EXISTS product_tags (
    id serial primary key,
    product_id INT DEFAULT NULL,
    tag VARCHAR(50) DEFAULT NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id) REFERENCES products(id)
);
