package controller

import (
	"test-actionpay/exception"
	"test-actionpay/src/product/service"
	"test-actionpay/utils"

	"github.com/gofiber/fiber/v2"
)

type productController struct {
	productService service.ProductService
}

func NewProductController(productService service.ProductService) *productController {
	return &productController{productService}
}

func (controller *productController) Route(app fiber.Router) {
	productV0 := app.Group("/v0/product")
	// mmh
	productV0.Get("/", controller.getProducts)
	productV0.Get("/:id", controller.getProductByID)
	productV0.Get("/:slug/slug", controller.getProductBySlug)
}

func (controller *productController) getProducts(c *fiber.Ctx) error {
	q := utils.GeneralRequest{}
	err := c.QueryParser(&q)

	if err != nil {
		exception.PanicIfNeeded(exception.ServerError{
			Message: err.Error(),
		})
	}

	response, manifest, err := controller.productService.GetProducts(q)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebArrayResponse{
		Code:     200,
		Status:   "success",
		Message:  "Successfull",
		Data:     response,
		Manifest: manifest,
	})
}

func (controller *productController) getProductByID(c *fiber.Ctx) error {
	productId := c.Params("id")
	if productId == ":id" {
		productId = ""
	}

	response, err := controller.productService.GetProductByID(productId)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    200,
		Status:  "success",
		Message: "Successfull",
		Data:    response,
	})
}

func (controller *productController) getProductBySlug(c *fiber.Ctx) error {
	productSlug := c.Params("slug")
	if productSlug == ":slug" {
		productSlug = ""
	}

	response, err := controller.productService.GetProductBySlug(productSlug)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	return c.JSON(utils.WebResponse{
		Code:    200,
		Status:  "success",
		Message: "Successfull",
		Data:    response,
	})
}
