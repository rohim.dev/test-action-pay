package repository

import (
	"fmt"
	"strings"
	"test-actionpay/config"
	"test-actionpay/src/product/entity"
	"test-actionpay/utils"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

func NewProductRepository(db *sqlx.DB) ProductRepository {
	return &productRepositoryImpl{db}
}

type productRepositoryImpl struct {
	db *sqlx.DB
}

func (r *productRepositoryImpl) GetProducts(req utils.GeneralEntity) (response []entity.Product, pagination utils.Pagination, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Product{}
	pagination = utils.Pagination{}

	statement := sq.Select().
		From("products as a").
		PlaceholderFormat(sq.Dollar)

	if req.Search != "" {
		statement = statement.Where(sq.Or{
			sq.Like{
				"a.title": fmt.Sprintf("%%%s%%", req.Search),
			},
		})
	}

	//prepare for pagination
	pageStatement := statement.Columns("COUNT(1) as pagination_total")
	pageSql, pageArgs, err := pageStatement.ToSql()
	if err != nil {
		return
	}

	err = r.db.GetContext(ctx, &pagination, pageSql, pageArgs...)
	if err != nil {
		return
	}

	pagination.Page = req.Page
	pagination.Limit = req.Limit

	page := (req.Page - 1) * req.Limit
	statement = statement.Limit(uint64(req.Limit)).Offset(uint64(page))

	sortField := map[string]utils.SortBy{
		"id": {
			Fieldname: "a.id",
			Value:     "asc",
		},
		"-id": {
			Fieldname: "a.id",
			Value:     "desc",
		},
		"name": {
			Fieldname: "a.title",
			Value:     "asc",
		},
		"-name": {
			Fieldname: "a.title",
			Value:     "desc",
		},
		"stock": {
			Fieldname: "a.stock",
			Value:     "asc",
		},
		"-stock": {
			Fieldname: "a.stock",
			Value:     "desc",
		},
		"price": {
			Fieldname: "a.price",
			Value:     "asc",
		},
		"-price": {
			Fieldname: "a.price",
			Value:     "desc",
		},
		"created_at": {
			Fieldname: "a.created_at",
			Value:     "asc",
		},
		"-created_at": {
			Fieldname: "a.created_at",
			Value:     "desc",
		},
	}

	if req.Sort == "" {
		req.Sort = "-created_at"
	}

	statement = statement.Columns("a.id,a.slug,a.title,a.description,a.category,a.price,a.discount_percentage,a.rating,a.stock,a.brand,a.sku,a.weight,a.warranty_information,a.shipping_information,a.availability_status,a.return_policy,a.minimum_order_quantity,a.thumbnail, a.created_at, a.updated_at, a.barcode, a.qr_code")
	statement = statement.OrderBy(fmt.Sprintf("%s %s", sortField[req.Sort].Fieldname, sortField[req.Sort].Value))
	sql, args, err := statement.ToSql()
	fmt.Println(sql)
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Product
		err := rows.StructScan(&item)
		if err != nil {
			return response, pagination, err
		}

		response = append(response, item)
	}

	return response, pagination, nil
}

func (r *productRepositoryImpl) FindProductByID(id int) (*entity.Product, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Select("a.id,a.slug,a.title,a.description,a.category,a.price,a.discount_percentage,a.rating,a.stock,a.brand,a.sku,a.weight,a.warranty_information,a.shipping_information,a.availability_status,a.return_policy,a.minimum_order_quantity,a.thumbnail, a.created_at, a.updated_at, a.barcode, a.qr_code").
		From("products a").
		Where(sq.Eq{
			"id": id,
		}).PlaceholderFormat(sq.Dollar)

	querySql, queryArgs, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	var response entity.Product
	if err := r.db.GetContext(ctx, &response, querySql, queryArgs...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, err
}

func (r *productRepositoryImpl) FindProductBySlug(slug string) (*entity.Product, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Select("a.id,a.slug,a.title,a.description,a.category,a.price,a.discount_percentage,a.rating,a.stock,a.brand,a.sku,a.weight,a.warranty_information,a.shipping_information,a.availability_status,a.return_policy,a.minimum_order_quantity,a.thumbnail, a.created_at, a.updated_at, a.barcode, a.qr_code").
		From("products a").
		Where(sq.Eq{
			"slug": slug,
		}).PlaceholderFormat(sq.Dollar)

	querySql, queryArgs, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	var response entity.Product
	if err := r.db.GetContext(ctx, &response, querySql, queryArgs...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, err
}

func (r *productRepositoryImpl) FindDimensionProductByID(id int) (*entity.Dimensions, error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	q := sq.Select("a.id, a.product_id, a.width, a.height, a.depth").
		From("product_dimensions a").
		Where(sq.Eq{
			"product_id": id,
		}).PlaceholderFormat(sq.Dollar)

	querySql, queryArgs, err := q.ToSql()
	if err != nil {
		return nil, err
	}

	var response entity.Dimensions
	if err := r.db.GetContext(ctx, &response, querySql, queryArgs...); err != nil {
		if strings.Contains(err.Error(), "sql: no rows in result set") {
			return nil, nil
		}

		return nil, err
	}

	return &response, err
}

func (r *productRepositoryImpl) FindTagByProductID(productID int) (response []entity.Tag, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Tag{}

	statement := sq.Select().
		From("product_tags as a").
		Where(sq.Eq{
			"a.product_id": productID,
		}).
		PlaceholderFormat(sq.Dollar)

	statement = statement.Columns("a.id, a.tag, a.product_id, a.created_at, a.updated_at")
	statement = statement.OrderBy("a.created_at desc")
	sql, args, err := statement.ToSql()
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Tag
		err := rows.StructScan(&item)
		if err != nil {
			return response, err
		}

		response = append(response, item)
	}

	return response, nil
}

func (r *productRepositoryImpl) FindReviewByProductID(productID int) (response []entity.Review, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Review{}

	statement := sq.Select().
		From("product_reviews as a").
		Where(sq.Eq{
			"a.product_id": productID,
		}).
		PlaceholderFormat(sq.Dollar)

	statement = statement.Columns("a.id, a.rating, a.comment, reviewer_name, a.reviewer_email, a.product_id, a.created_at, a.updated_at")
	statement = statement.OrderBy("a.created_at desc")
	sql, args, err := statement.ToSql()
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Review
		err := rows.StructScan(&item)
		if err != nil {
			return response, err
		}

		response = append(response, item)
	}

	return response, nil
}

func (r *productRepositoryImpl) FindImageByProductID(productID int) (response []entity.Image, err error) {
	ctx, cancel := config.NewPostgreSqlxContext()
	defer cancel()

	response = []entity.Image{}

	statement := sq.Select().
		From("product_images as a").
		Where(sq.Eq{
			"a.product_id": productID,
		}).
		PlaceholderFormat(sq.Dollar)

	statement = statement.Columns("a.id, a.image_url, a.product_id, a.created_at, a.updated_at")
	statement = statement.OrderBy("a.created_at desc")
	sql, args, err := statement.ToSql()
	if err != nil {
		return
	}

	rows, err := r.db.QueryxContext(ctx, sql, args...)
	if err != nil {
		return
	}

	defer rows.Close()

	for rows.Next() {
		var item entity.Image
		err := rows.StructScan(&item)
		if err != nil {
			return response, err
		}

		response = append(response, item)
	}

	return response, nil
}
