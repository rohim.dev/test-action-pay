package repository

import (
	"test-actionpay/src/product/entity"
	"test-actionpay/utils"
)

type ProductRepository interface {
	GetProducts(req utils.GeneralEntity) (response []entity.Product, pagination utils.Pagination, err error)
	FindProductByID(id int) (*entity.Product, error)
	FindProductBySlug(slug string) (*entity.Product, error)
	FindDimensionProductByID(id int) (*entity.Dimensions, error)
	FindTagByProductID(productID int) (response []entity.Tag, err error)
	FindReviewByProductID(productID int) (response []entity.Review, err error)
	FindImageByProductID(productID int) (response []entity.Image, err error)
}
