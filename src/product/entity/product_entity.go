package entity

import (
	"test-actionpay/utils"
)

type Product struct {
	ID                   int     `db:"id"`
	Title                string  `db:"title"`
	Slug                 string  `db:"slug"`
	Description          string  `db:"description"`
	Category             string  `db:"category"`
	Price                float64 `db:"price"`
	DiscountPercentage   float64 `db:"discount_percentage"`
	Rating               float64 `db:"rating"`
	Stock                int     `db:"stock"`
	Brand                string  `db:"brand"`
	SKU                  string  `db:"sku"`
	Weight               float64 `db:"weight"`
	WarrantyInformation  string  `db:"warranty_information"`
	ShippingInformation  string  `db:"shipping_information"`
	AvailabilityStatus   string  `db:"availability_status"`
	ReturnPolicy         string  `db:"return_policy"`
	MinimumOrderQuantity int     `db:"minimum_order_quantity"`
	Barcode              string  `db:"barcode"`
	QRCode               string  `db:"qr_code"`
	Thumbnail            string  `db:"thumbnail"`
	utils.Default
}

type Dimensions struct {
	ID        int     `db:"id"`
	ProductID int     `db:"product_id"`
	Width     float64 `db:"width"`
	Height    float64 `db:"height"`
	Depth     float64 `db:"depth"`
	utils.Default
}

type Review struct {
	ID            int     `db:"id"`
	ProductID     int     `db:"product_id"`
	Rating        float64 `db:"rating"`
	Comment       string  `db:"comment"`
	ReviewerName  string  `db:"reviewer_name"`
	ReviewerEmail string  `db:"reviewer_email"`
	utils.Default
}

type Image struct {
	ID        int    `db:"id"`
	ProductID int    `db:"product_id"`
	ImageUrl  string `db:"image_url"`
	utils.Default
}

type Tag struct {
	ID        int    `db:"id"`
	ProductID int    `db:"product_id"`
	Tag       string `db:"tag"`
	utils.Default
}
