package service

import (
	"errors"
	"strconv"
	"test-actionpay/exception"
	"test-actionpay/src/product/entity"
	"test-actionpay/src/product/model"
	"test-actionpay/src/product/repository"
	"test-actionpay/utils"
)

func NewProductService(
	productRepo repository.ProductRepository,
) ProductService {
	return &productServiceImpl{productRepo}
}

type productServiceImpl struct {
	productRepo repository.ProductRepository
}

func (s *productServiceImpl) GetProducts(request utils.GeneralRequest) (response []model.Product, manifest utils.ManifestResponse, err error) {
	response = []model.Product{}
	manifest = utils.ManifestResponse{}

	if request.Page == "" {
		request.Page = "1"
	}

	if request.Limit == "" {
		request.Limit = "10"
	}

	u64page, err := strconv.ParseUint(request.Page, 10, 32)
	if err != nil {
		return response, manifest, err
	}
	u64limit, err := strconv.ParseUint(request.Limit, 10, 32)
	if err != nil {
		return response, manifest, err
	}

	req := utils.GeneralEntity{
		Page:   uint(u64page),
		Limit:  uint(u64limit),
		Search: request.Search,
		Sort:   request.Sort,
	}

	results, pagination, err := s.productRepo.GetProducts(req)
	if err != nil {
		exception.PanicIfNeeded(exception.ClientError{
			Message: err.Error(),
		})
	}

	for _, v := range results {
		dimensionChan := make(chan *entity.Dimensions)
		go func() {
			defer close(dimensionChan)
			dimension, err := s.productRepo.FindDimensionProductByID(v.ID)
			if err != nil {
				dimensionChan <- nil
				return
			}

			dimensionChan <- dimension
		}()

		tagChan := make(chan []entity.Tag)
		go func() {
			defer close(tagChan)
			tag, err := s.productRepo.FindTagByProductID(v.ID)
			if err != nil {
				tagChan <- nil
				return
			}

			tagChan <- tag
		}()

		reviewChan := make(chan []entity.Review)
		go func() {
			defer close(reviewChan)
			review, err := s.productRepo.FindReviewByProductID(v.ID)
			if err != nil {
				reviewChan <- nil
				return
			}

			reviewChan <- review
		}()

		imageChan := make(chan []entity.Image)
		go func() {
			defer close(imageChan)
			image, err := s.productRepo.FindImageByProductID(v.ID)
			if err != nil {
				imageChan <- nil
				return
			}

			imageChan <- image
		}()

		tags := []string{}
		for _, v := range <-tagChan {
			tags = append(tags, v.Tag)
		}
		reviews := []model.Review{}
		for _, v := range <-reviewChan {
			reviews = append(reviews, model.Review{
				Rating:        v.Rating,
				Comment:       v.Comment,
				Date:          v.CreatedAt,
				ReviewerName:  v.ReviewerName,
				ReviewerEmail: v.ReviewerEmail,
			})
		}
		images := []string{}
		for _, v := range <-imageChan {
			images = append(images, v.ImageUrl)
		}
		dimen := <-dimensionChan

		item := model.Product{
			ID:                 v.ID,
			Title:              v.Title,
			Description:        v.Description,
			Slug:               v.Slug,
			Category:           v.Category,
			Price:              v.Price,
			DiscountPercentage: v.DiscountPercentage,
			Rating:             v.Rating,
			Stock:              v.Stock,
			Tags:               tags,
			Brand:              v.Brand,
			SKU:                v.SKU,
			Weight:             v.Weight,
			Dimensions: model.Dimensions{
				Width:  dimen.Width,
				Height: dimen.Height,
				Depth:  dimen.Depth,
			},
			WarrantyInformation:  v.WarrantyInformation,
			ShippingInformation:  v.ShippingInformation,
			AvailabilityStatus:   v.AvailabilityStatus,
			Reviews:              reviews,
			ReturnPolicy:         v.ReturnPolicy,
			MinimumOrderQuantity: v.MinimumOrderQuantity,
			Meta: model.Meta{
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				Barcode:   v.Barcode,
				QRCode:    v.QRCode,
			},
			Images:    images,
			Thumbnail: v.Thumbnail,
		}

		response = append(response, item)

	}

	if len(response) == 0 {
		response = nil
	}

	manifest = utils.ManifestResponse(pagination)

	return response, manifest, nil
}

func (s *productServiceImpl) GetProductByID(id string) (response model.Product, err error) {
	response = model.Product{}
	if id == "" {
		return response, errors.New("product id cannot be empty")
	}

	strId2Int, err := strconv.Atoi(id)
	if err != nil {
		return response, err
	}

	product, err := s.productRepo.FindProductByID(strId2Int)
	if err != nil {
		return response, err
	}

	if product == nil {
		return response, errors.New("product cannot be found")
	}

	dimensionChan := make(chan *entity.Dimensions)
	go func() {
		defer close(dimensionChan)
		dimension, err := s.productRepo.FindDimensionProductByID(strId2Int)
		if err != nil {
			dimensionChan <- nil
			return
		}

		dimensionChan <- dimension
	}()

	tagChan := make(chan []entity.Tag)
	go func() {
		defer close(tagChan)
		tag, err := s.productRepo.FindTagByProductID(strId2Int)
		if err != nil {
			tagChan <- nil
			return
		}

		tagChan <- tag
	}()

	reviewChan := make(chan []entity.Review)
	go func() {
		defer close(reviewChan)
		review, err := s.productRepo.FindReviewByProductID(strId2Int)
		if err != nil {
			reviewChan <- nil
			return
		}

		reviewChan <- review
	}()

	imageChan := make(chan []entity.Image)
	go func() {
		defer close(imageChan)
		image, err := s.productRepo.FindImageByProductID(strId2Int)
		if err != nil {
			imageChan <- nil
			return
		}

		imageChan <- image
	}()

	tags := []string{}
	for _, v := range <-tagChan {
		tags = append(tags, v.Tag)
	}
	reviews := []model.Review{}
	for _, v := range <-reviewChan {
		reviews = append(reviews, model.Review{
			Rating:        v.Rating,
			Comment:       v.Comment,
			Date:          v.CreatedAt,
			ReviewerName:  v.ReviewerName,
			ReviewerEmail: v.ReviewerEmail,
		})
	}
	images := []string{}
	for _, v := range <-imageChan {
		images = append(images, v.ImageUrl)
	}
	dimen := <-dimensionChan

	response = model.Product{
		ID:                 product.ID,
		Title:              product.Title,
		Description:        product.Description,
		Slug:               product.Slug,
		Category:           product.Category,
		Price:              product.Price,
		DiscountPercentage: product.DiscountPercentage,
		Rating:             product.Rating,
		Stock:              product.Stock,
		Tags:               tags,
		Brand:              product.Brand,
		SKU:                product.SKU,
		Weight:             product.Weight,
		Dimensions: model.Dimensions{
			Width:  dimen.Width,
			Height: dimen.Height,
			Depth:  dimen.Depth,
		},
		WarrantyInformation:  product.WarrantyInformation,
		ShippingInformation:  product.ShippingInformation,
		AvailabilityStatus:   product.AvailabilityStatus,
		Reviews:              reviews,
		ReturnPolicy:         product.ReturnPolicy,
		MinimumOrderQuantity: product.MinimumOrderQuantity,
		Meta: model.Meta{
			CreatedAt: product.CreatedAt,
			UpdatedAt: product.UpdatedAt,
			Barcode:   product.Barcode,
			QRCode:    product.QRCode,
		},
		Images:    images,
		Thumbnail: product.Thumbnail,
	}

	return
}

func (s *productServiceImpl) GetProductBySlug(slug string) (response model.Product, err error) {
	response = model.Product{}
	if slug == "" {
		return response, errors.New("product id cannot be empty")
	}

	product, err := s.productRepo.FindProductBySlug(slug)
	if err != nil {
		return response, err
	}

	if product == nil {
		return response, errors.New("product cannot be found")
	}

	dimensionChan := make(chan *entity.Dimensions)
	go func() {
		defer close(dimensionChan)
		dimension, err := s.productRepo.FindDimensionProductByID(product.ID)
		if err != nil {
			dimensionChan <- nil
			return
		}

		dimensionChan <- dimension
	}()

	tagChan := make(chan []entity.Tag)
	go func() {
		defer close(tagChan)
		tag, err := s.productRepo.FindTagByProductID(product.ID)
		if err != nil {
			tagChan <- nil
			return
		}

		tagChan <- tag
	}()

	reviewChan := make(chan []entity.Review)
	go func() {
		defer close(reviewChan)
		review, err := s.productRepo.FindReviewByProductID(product.ID)
		if err != nil {
			reviewChan <- nil
			return
		}

		reviewChan <- review
	}()

	imageChan := make(chan []entity.Image)
	go func() {
		defer close(imageChan)
		image, err := s.productRepo.FindImageByProductID(product.ID)
		if err != nil {
			imageChan <- nil
			return
		}

		imageChan <- image
	}()

	tags := []string{}
	for _, v := range <-tagChan {
		tags = append(tags, v.Tag)
	}
	reviews := []model.Review{}
	for _, v := range <-reviewChan {
		reviews = append(reviews, model.Review{
			Rating:        v.Rating,
			Comment:       v.Comment,
			Date:          v.CreatedAt,
			ReviewerName:  v.ReviewerName,
			ReviewerEmail: v.ReviewerEmail,
		})
	}
	images := []string{}
	for _, v := range <-imageChan {
		images = append(images, v.ImageUrl)
	}
	dimen := <-dimensionChan

	response = model.Product{
		ID:                 product.ID,
		Title:              product.Title,
		Description:        product.Description,
		Slug:               product.Slug,
		Category:           product.Category,
		Price:              product.Price,
		DiscountPercentage: product.DiscountPercentage,
		Rating:             product.Rating,
		Stock:              product.Stock,
		Tags:               tags,
		Brand:              product.Brand,
		SKU:                product.SKU,
		Weight:             product.Weight,
		Dimensions: model.Dimensions{
			Width:  dimen.Width,
			Height: dimen.Height,
			Depth:  dimen.Depth,
		},
		WarrantyInformation:  product.WarrantyInformation,
		ShippingInformation:  product.ShippingInformation,
		AvailabilityStatus:   product.AvailabilityStatus,
		Reviews:              reviews,
		ReturnPolicy:         product.ReturnPolicy,
		MinimumOrderQuantity: product.MinimumOrderQuantity,
		Meta: model.Meta{
			CreatedAt: product.CreatedAt,
			UpdatedAt: product.UpdatedAt,
			Barcode:   product.Barcode,
			QRCode:    product.QRCode,
		},
		Images:    images,
		Thumbnail: product.Thumbnail,
	}

	return
}
