package service

import (
	"test-actionpay/src/product/model"
	"test-actionpay/utils"
)

type ProductService interface {
	GetProducts(request utils.GeneralRequest) (response []model.Product, manifest utils.ManifestResponse, err error)
	GetProductByID(id string) (response model.Product, err error)
	GetProductBySlug(slug string) (response model.Product, err error)
}
